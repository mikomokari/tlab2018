import mnist_loader
import network
import time


## gzlファイル読み込み＆データ引渡し
training_data, validation_data, test_data = mnist_loader.load_data_wrapper()


## ニューラルネットワークの生成
net = network.Network([784, 30, 10])

## トータル時間計測開始
start_time = time.time()

net.SGD(training_data, 30, 10, 3.0, test_data=test_data)	## ネットワークスタート

end_time = time.time()
## トータル時間計測終了


## 時間計算処理
total_time = end_time - start_time
ms_time = (total_time * 1000)%1000
s_time = total_time%60
m_time = (total_time - s_time)/60


## 出力処理
print "Total time: %d:%d:%d"%(m_time, s_time, ms_time)
